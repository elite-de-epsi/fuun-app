import { Component, OnInit } from '@angular/core';
import {AuthService} from "@auth0/auth0-angular";
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  constructor(public auth: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  async login() {
    await this.auth.loginWithRedirect();
    await this.router.navigate(['/']);
  }

}
