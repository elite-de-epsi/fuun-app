import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  // @ts-ignore
  // tslint:disable-next-line:variable-name
  public choose;
  // @ts-ignore
  public label;
  // tslint:disable-next-line:variable-name

  constructor() { }

  ngOnInit(): void {

  }
  // tslint:disable-next-line:variable-name

  // @ts-ignore
  chooseType(e) {
    this.choose = e.target.value;
    this.label = e.target.options[e.target.options.selectedIndex].text;
    console.log(this.choose);
  }

}
