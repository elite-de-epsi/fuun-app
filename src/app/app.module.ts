import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { ProfilComponent } from './profil/profil.component';
import { PostComponent } from './post/post.component';
import { HomeComponent } from './home/home.component';
import {CommonModule} from "@angular/common";
import {AuthModule} from "@auth0/auth0-angular";

@NgModule({
  declarations: [
    AppComponent,
    ConnexionComponent,
    ProfilComponent,
    PostComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    AuthModule.forRoot({
      domain: 'dev-dd3ty71p.us.auth0.com',
      clientId: 'yyTVKkiyGld8vS5rTVAAaRabYfVN5XGP'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
