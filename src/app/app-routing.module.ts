import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {PostComponent} from "./post/post.component";
import {ProfilComponent} from "./profil/profil.component";
import {ConnexionComponent} from "./connexion/connexion.component";

const routes: Routes = [
  {path: 'login', component : ConnexionComponent},
  {path: '', component : HomeComponent},
  {path: 'post', component : PostComponent},
  {path: 'profil', component : ProfilComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
