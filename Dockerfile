FROM node:16.13.1 AS builder
WORKDIR /app
COPY . .
RUN npm install && npm run build

FROM nginx:1.21.6-alpine AS web
COPY nginx.conf /etc/nginx/
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/dist .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
